import javax.xml.crypto.Data;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {
    private ServerSocket serverSocket;
    private ObjectOutputStream dataOut;
    private ObjectInputStream dataIn;
    private static ArrayList <DataClient> dt;

    public Server() {
        dt = new ArrayList<>();
        try{ // initialise la serveur
            serverSocket = new ServerSocket(1080);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        Server server = new Server();
        while(true) {
            server.ecoute(); //lit les données envoyer par le client
            System.out.println("/******************************************************/");
            System.out.println(dt.toString());
            server.ecrit();
        }
    }

    private void ecoute() {
        Socket s = null;

        try {
            //attend la connexion d'un client
            s = serverSocket.accept();
            System.out.println("ecoute");
            // lit ce que le client envoit
            dataIn = new ObjectInputStream(s.getInputStream());
            DataClient client = (DataClient) dataIn.readObject();

            //ajout du client dans l'arraylist de DataClient
            if(dt.size() > 0 ){
                boolean test = false;
                int i = -1;
                //verifie si le client existe
                for (DataClient d : dt) {
                    i++;
                    if(d.getNumber() == client.getNumber() && d.getTeam() == client.getTeam()){
                        //client deja ajouter a l'arraylist
                        System.out.println("OUI!!!!");
                        test = false;
                    }else{
                        //client pas encore ajouter a l'arraylist
                        System.out.println("NON");
                        test = true;
                    }
                }

                if(test){
                    dt.add(client); //ajoute un client
                }else{
                    dt.set(i, client); // met a jour un client
                }

            }else{
                dt.add(client); // ajoute le premier client
            }

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void ecrit(){
        Socket socket = null;

        try {
            //attend la connexion d'un client
            socket = serverSocket.accept();
            System.out.println("ecrire");
            dataOut = new ObjectOutputStream(socket.getOutputStream());
            DataClient c = dt.get(0);
            c.setJouer(true);
            //envoie les données au client et di au joueur qu'il peut jouer
            dataOut.writeObject(c);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
