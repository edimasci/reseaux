import java.io.*;
import java.net.Socket;

public class ClientTest {
    private Socket socket;
    private ObjectOutputStream dataOut;
    private ObjectInputStream dataIn;
    private DataClient dc;

    public ClientTest(DataClient dc) {
        this.dc = dc;
    }

    public static void main(String[] args) {
        //initialise les donné client
        DataClient c = new DataClient(2, 1, 2, 2);
        ClientTest ct = new ClientTest(c);

        while (true){
            ct.ecrire();//envoie les données au serveur
            ct.lecture();//lit les données que le serveur envoie
        }
    }

    private void ecrire(){
        try {
            socket = new Socket("127.0.0.1",1080); // se connecte sur le port 1080
            System.out.println("Lire");
            dataOut = new ObjectOutputStream(socket.getOutputStream());
            dc.setX(dc.getX()+1);
            dataOut.writeObject(dc); //envoie l'objet DataClient
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void lecture(){
        int i = 0;
        while (!dc.isJouer()){
            i++;
            try {
                socket = new Socket("127.0.0.1",1080);
                System.out.println("ecrire");
                dataIn = new ObjectInputStream(socket.getInputStream());
                dc = (DataClient) dataIn.readObject(); //met a jour dc avec les données recu par le serveur
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        dc.setJouer(false);
    }
}
