import java.io.Serializable;
import java.util.Arrays;

public class DataClient implements Serializable {
    byte[] etat = new byte[2 * 10 * 10];
    int team;
    int x;
    int y;
    int number;
    boolean jouer = false;
    int port = 2000;

    public DataClient(int team, int number, int x, int y) {
        this.team = team;
        this.x = x;
        this.y = y;
        this.number = number;
    }

    public boolean isJouer() {
        return jouer;
    }

    public void setJouer(boolean jouer) {
        this.jouer = jouer;
    }

    public byte[] getEtat() {
        return etat;
    }

    public void setEtat(byte[] etat) {
        this.etat = etat;
    }

    public int getTeam() {
        return team;
    }

    public void setTeam(int team) {
        this.team = team;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return "dataClient{" +
                ", team=" + team +
                ", x=" + x +
                ", y=" + y +
                ", number=" + number +
                ", port=" + port +
                '}';
    }
}
