import java.awt.Frame;
import java.awt.BorderLayout;
import java.awt.event.WindowAdapter; // Window Event
import java.awt.event.WindowEvent; // Window Event

import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;


import java.io.DataOutputStream;
import java.io.IOException;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.BufferedReader;

/**
 * @author Titre
 */
public class TPClient extends Frame {

    byte[] etat = new byte[2 * 10 * 10];
    int team;
    int x;
    int y;
    int number;
    int port = 2000;
    Socket socket = null;
    InputStream in;
    DataOutputStream out;
    TPPanel tpPanel;
    TPCanvas tpCanvas;
    Timer timer;

    /**
     * Constructeur
     */
    public TPClient(int number, int team, int x, int y) {
        setLayout(new BorderLayout());
        tpPanel = new TPPanel(this);
        add("North", tpPanel);
        tpCanvas = new TPCanvas(this.etat);
        add("Center", tpCanvas);

        timer = new Timer();
        timer.schedule(new MyTimerTask(), 500, 500);

        this.number = number;

    }

    /**
     * Action vers droit
     */
    public synchronized void droit() {
        // verification collisions mur droit

        if (this.x < tpCanvas.nbPosition-1) {
            this.x += 1;
        }
        System.out.println("Droit");
        tpCanvas.repaint();
    }

    /**
     * Action vers gauche
     */
    public synchronized void gauche() {
        // verification collisions mur gauche
        if(this.x > 0) {
            this.x -= 1;
        }
        System.out.println("Gauche");
        tpCanvas.repaint();
    }

    /**
     * Action vers gauche
     */
    public synchronized void haut() {
        // verification collisions plafond
        if(this.y > 0) {
            this.y -= 1;
        }
        System.out.println("Haut");
        tpCanvas.repaint();
    }

    /**
     * Action vers bas
     */
    public synchronized void bas() {
        // verification collisions en bas du canvas
        if(this.y < tpCanvas.nbPosition-1) {
            this.y += 1;
        }
        System.out.println("Bas");
        tpCanvas.repaint();
    }

    /**
     * Pour rafraichir la situation
     */
    public synchronized void refresh() {
        tpCanvas.repaint();
        this.tpCanvas.paintCarte(this.tpCanvas.getGraphics());
        this.tpCanvas.drawPlayer(this.tpCanvas.getGraphics(), this.x, this.y, (byte) this.team);
    }

    /**
     * Pour recevoir l'Etat
     */
    public void receiveEtat() {

    }

    /**
     * Initialisations
     */
    public void minit(int number, int pteam, int px, int py) throws IOException {
        this.x = px-1;
        this.y = py-1;
        this.team = pteam;

        //permet d'envoyer les donner de base au serv
        /*Socket client = new Socket("127.0.0.1", 1024);

        String arg[] = {Integer.toString(number), Integer.toString(pteam), Integer.toString(px), Integer.toString(py)};
        //envoi de données
        DataOutputStream out = new DataOutputStream(client.getOutputStream());
        for (int i=0; i<4;i++) {
            out.writeUTF(arg[i]);
        }*/
        tpCanvas.paintCarte(this.tpCanvas.getGraphics());
        tpCanvas.drawPlayer(getGraphics(),x,y,(byte) team);

    }

    public String etat() {
        String result = new String();
        return result;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {

        System.out.println("args :" + args.length);

        if (args.length != 4) {
            System.out.println("Usage : java TPClient number color positionX positionY ");
            System.exit(0);
        }

        int number = Integer.parseInt(args[0]);
        int team = Integer.parseInt(args[1]);
        int x = Integer.parseInt(args[2]);
        int y = Integer.parseInt(args[3]);


        try {
            TPClient tPClient = new TPClient(number, team, x, y);
            // Pour fermeture
            tPClient.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    System.exit(0);
                }
            });

            // Create Panel back forward

            tPClient.pack();
            tPClient.setSize(1000, 1000 + 200);
            tPClient.setVisible(true);
            tPClient.minit(number, team, x, y);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Pour rafraichir
     */
    class MyTimerTask extends TimerTask {

        public void run() {
            refresh();
        }
    }

}
