# Réseaux

## récupere le projet

vous pouvez telecharger le projet en fichier comprésser (zip, tar, ...)
Ensuite vous pouvez le decompreser dans le fichier que vous voulez decompresser sa ou vous le souhaitez

Si vous avez Git d'installer sur votre machine vous pouvez cloner le projet dans le fichier que vous voulez

---
## Installer le projet
#### Prerequis
vous devez avoir OpenJDK 11 d'installer sur votre machine
#### Compilation
```
cd tea/src
javac Server.java
javac ClientTest.java
javac TPClient.java
```
---
## Executer le code
#### Communication Client-Serveur
```
java Server
java Client
```
#### Afficher l'interface
```
java TPClient
```
